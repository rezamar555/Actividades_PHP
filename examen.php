<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<SCRIPT LANGUAGE="JavaScript">
/*
var res = new Array;
var resdos = new Array;
var tuRespuesta = new Array;
var puntuacion = 0;
 
res[1] = "a";
res[2] = "c";
res[3] = "b";
 
resdos[1]="La respuesta corecta era A\n";
resdos[2]="La respuesta corecta era C\n";
resdos[3]="La respuesta corecta era B\n";
 
function Motor(pregunta, res)
{
   tuRespuesta[pregunta]=res ;
}
 
function Puntuacion()
{
var textoRespuesta = "Resultados:\n" ;
puntuacion=0 ;

for(i=1;i<=9;i++)
{
   textoRespuesta=textoRespuesta+"Pregunta "+i+": " ;
   if(res[i] != tuRespuesta[i])
   {
      textoRespuesta=textoRespuesta+resdos[i] ;
   }
   else
   {
      textoRespuesta=textoRespuesta+"Felicidades\n" ;
      puntuacion++ ;
   }
}
 
textoRespuesta=textoRespuesta+"PUNTUACION FINAL : "+puntuacion+"\n" ;
 
textoRespuesta=textoRespuesta+"Comentario : " ;
if(puntuacion <= 4)
{
	textoRespuesta=textoRespuesta+"Examen no aprobado" ;
}

if(puntuacion >= 5 && puntuacion <= 6)
{
textoRespuesta=textoRespuesta+"Regular" ;
}
if(puntuacion >= 7 && puntuacion <= 8)
{
textoRespuesta=textoRespuesta+"Felicidades" ;
}
if(puntuacion > 8)
{
textoRespuesta=textoRespuesta+"Excelente Promedio" ;
}
 
alert(textoRespuesta);
}
*/
</SCRIPT>


<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/Style.css">
<script src="js/jquery-3.2.1.js"></script>
<script src="js/bootstrap.min.js"></script>
<link rel="stylesheet" href="css/stylex.css">
<nav class="navbar navbar-default" role="nagivation">
	<div class="navigation-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
	</div>
	<div class="navbar-collapse collapse">
		<ul class="nav navbar-nav navbar-left">
			<li><a href="act.php">Inicio</a></li>
			<li><a href="MinusMayus.php">Minuscula a Mayuscula</a></li>
			<li><a href="pal.php">Palindromo</a></li>
			<li><a href="zod.php">Signo Zodiacal</a></li>
			<li><a href="examen.php">Examen</a></li>
			<li><a href="Reg.php">Formulario</a></li>
		</ul>
	</div>
</nav>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" type="text/css" media="all" href="css/estilo01.css" title="Normal" />
    <link rel="stylesheet" type="text/css" media="all" href="css/botones.css" title="Normal" />
    <title>
        Examen de: Validacion con PHP </title>
    <style type="text/css">
    
    </style>
</head>

<body>
    <!-- id=''|lang='' MAVG 2015/09/21 -->
    <h2>
	PHP a Validar!	</h2>
    <form method="POST">
        <div id="contenido">
            <div class="pregunta">
                <h3>Pregunta 1:</h3>
                <div>
                    <p>En JavaScript, �c�mo se inserta un comentario que ocupa una l�nea?</p>
                    <input type="radio" value="a" name="p1"/>
                    <span>a) </span>
                    <span># Comentario</span>
                    <br/>
                    <br/>
                    <input type="radio" value="b" name="p1"/>
                    <span>b) </span>
                    <span>// Comentario</span>
                    <br/>
                    <br/>
                    <input type="radio" value="c" name="p1"/>
                    <span>c) </span>
                    <span>' Comentario</span>
                    <br/>
                    <br/>
                    <input type="radio" value="d" name="p1"/>
                    <span>d) </span>
                    <span>Las anteriores respuestas no son correctas</span>
                    <br/>
                    <br/>
                </div>
            </div>
            <div class="pregunta">
                <h3>Pregunta 2:</h3>
                <div>
                    <p>En el DOM, para eliminar un elemento hijo se emplea el m�todo</p>
                    <input type="radio" value="a" name="p2"/>
                    <span>a) </span>
                    <span>deleteChild()</span>
                    <br/>
                    <br/>
                    <input type="radio" value="b" name="p2"/>
                    <span>b) </span>
                    <span>dropChild()</span>
                    <br/>
                    <br/>
                    <input type="radio" value="c" name="p2"/>
                    <span>c) </span>
                    <span>removeChild()</span>
                    <br/>
                    <br/>
                    <input type="radio" value="d" name="p2"/>
                    <span>d) </span>
                    <span>Las anteriores respuestas no son correctas</span>
                    <br/>
                    <br/>
                </div>
            </div>
            <div class="pregunta">
                <h3>Pregunta 3:</h3>
                <div>
                    <p>�Qu� etiqueta de HTML se emplea para escribir c�digo JavaScript?</p>
                    <input type="radio" value="a" name="p3"/>
                    <span>a) </span>
                    <span>&lt;script&gt;</span>
                    <br/>
                    <br/>
                    <input type="radio" value="b" name="p3"/>
                    <span>b) </span>
                    <span>&lt;javascript&gt;</span>
                    <br/>
                    <br/>
                    <input type="radio" value="c" name="p3"/>
                    <span>c) </span>
                    <span>&lt;scripting&gt;</span>
                    <br/>
                    <br/>
                    <input type="radio" value="d" name="p3"/>
                    <span>d) </span>
                    <span>&lt;js&gt;</span>
                    <br/>
                    <br/>
                </div>
            </div>
        <div id="pie">
			<input type="button" name="Evaluar "value="Evaluar"/>
            <input type="submit" value="Corregir examen" /> <a href="#" class="boton">Inicio</a>
            <input type="reset" value="Borrar respuestas" /> </div>         
    </form>
    
    <?php
    $total = 0 ;
    $p1 = $_POST['p1'];
 
    if(isset($_POST['Evaluar']))
    {  
		{
		echo "<h2>El resultado del examen es: $p1</h2>" ;
		/*if($_POST['p1'] == 'a' && $_POST['p2'] == 'b' && $_POST['p3'] == 'c') 
		{
		$total = 100 ;
		*/
		}
	}
	
    ?>
</body>
</html>
